import { except } from "@b08/array";
import { GeneratorOptions } from "./types";

const importRegex = /import\s+{(.+)}\s+from\s+["|']([^"']+)["|']/;

export function removeDuplicates(imports: string[], options: GeneratorOptions): string[] {
  const map = new Map<string, string[]>();
  const result: string[] = [];
  for (let i of imports) {
    const content = getImportContent(i);
    if (content == null) {
      result.push(i);
      continue;
    }
    const types = map.get(content.path);
    if (types == null) {
      map.set(content.path, content.types);
      result.push(i);
      continue;
    }

    const filtered = except(content.types, types);
    if (filtered.length > 0) {
      const replaced = filtered.length === content.types.length
      ? i
      : i.replace(importRegex, `import { ${filtered.join(", ")} } from ${options.quotes}${content.path}${options.quotes}`);
      result.push(replaced);
      map.set(content.path, [...types, ...filtered]);
    }
  }
  return result;
}

interface ImportContent {
  path: string;
  types: string[];
}

function getImportContent(imp: string): ImportContent {
  const match = imp.match(importRegex);
  if (match == null) { return null; }
  return {
    path: match[2],
    types: match[1].split(",").map(t => t.trim())
  };
}
