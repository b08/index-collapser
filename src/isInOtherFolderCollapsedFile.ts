import { KeyGroup } from "@b08/flat-key";
import { ContentFile } from "./types";
import { last, trimOne } from "@b08/array";

interface AbsPath {
  folder: string;
  isModulesPath: boolean;
  file: string;
}

export function isInOtherFolderCollapsedFile(file: ContentFile, importPath: string, byFolder: KeyGroup<string, ContentFile>): boolean {
  const abs = getAbsoluteFolderPath(importPath, file.folder);
  if (abs.isModulesPath) { return false; }
  const folder = byFolder.get(abs.folder);
  if (folder == null) { return false; }
  return folder.some(f => f.name === abs.file);
}

function getSplit(path: string): string[] {
  return path.split(/[\\\/]+/).filter(s => s.length > 0);
}

export function combinePath(...parts: string[]): string {
  return parts.filter(x => x.length > 0).join("/").replace(/\/+/g, "/");
}


function createPathFromSplit(split: string[], slashed: boolean): AbsPath {
  const folder = combinePath(...trimOne(split));
  return {
    file: last(split),
    folder: slashed ? "/" + folder : folder,
    isModulesPath: false
  };
}

function createAbsolutePath(split: string[], fileFolder: string): AbsPath {
  const slashed = fileFolder.startsWith("/");
  let splitFolder = getSplit(fileFolder);
  let index = 0;
  while (index < split.length) {
    const token = split[index];
    switch (token) {
      case ".": break;
      case "..":
        if (splitFolder.length > 1) {
          splitFolder = trimOne(splitFolder);
        }
        break;
      default:
        splitFolder = [...splitFolder, token];
    }
    index++;
  }
  if (last(split) === "." || last(split) === "..") {
    return createPathFromSplit([...splitFolder, "index"], slashed);
  }
  return createPathFromSplit(splitFolder, slashed);
}


export function getAbsoluteFolderPath(importPath: string, fileFolder: string): AbsPath {
  const split = getSplit(importPath);
  if (split.length === 0) { return null; }

  if (split[0] === "." || split[0] === "..") {
    return createAbsolutePath(split, fileFolder);
  }

  return { file: null, folder: importPath, isModulesPath: true };
}
