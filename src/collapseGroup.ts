import { flatMap } from "@b08/array";
import { KeyGroup } from "@b08/flat-key";
import { getFileImports } from "./getFileImports";
import { importsRegex } from "./importsRegex";
import { removeDuplicates } from "./removeDuplicates";
import { GeneratorOptions, ContentFile } from "./types";

export function collapseGroup(grp: ContentFile[], byFolder: KeyGroup<string, ContentFile>, options: GeneratorOptions): ContentFile {
  const imports = flatMap(grp, file => getFileImports(file, byFolder));
  const uniqueImports = removeDuplicates(imports, options);
  const fileBodies = grp.map(file => getFileBody(file));
  const regexStart = new RegExp(`^${options.linefeed}+`, "g");
  const double = options.linefeed + options.linefeed;
  const regexMultiple = new RegExp(`${double}+`, "g");
  const contents = [...uniqueImports, ...fileBodies].join(options.linefeed)
    .replace(regexStart, "")
    .replace(regexMultiple, double);
  return {
    ...grp[0],
    name: "index",
    extension: ".ts",
    contents
  };
}

function getFileBody(file: ContentFile): string {
  return file.contents.replace(importsRegex, "");
}
