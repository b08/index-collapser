import { KeyGroup } from "@b08/flat-key";
import { ContentFile } from "./types";
import { regexToArray } from "@b08/array";
import { isInOtherFolderCollapsedFile } from "./isInOtherFolderCollapsedFile";
import { importsRegex } from "./importsRegex";

export function getFileImports(file: ContentFile, byFolder: KeyGroup<string, ContentFile>): string[] {
  const found = regexToArray(importsRegex, file.contents);
  const notCollapsed = found.filter(f => !isInNeighborCollapsedFile(file, f[1], byFolder));
  return notCollapsed.map(f => tryReplaceImportWithIndex(file, f[0], f[1], byFolder));
}

function tryReplaceImportWithIndex(file: ContentFile, importLine: string, importPath: string, byFolder: KeyGroup<string, ContentFile>): string {
  if (!isInOtherFolderCollapsedFile(file, importPath, byFolder)) { return importLine; }
  return importLine.replace(importPath, cutLastSegment(importPath));
}


const neighborImportRegex = /^\.\/([^\/]+)$/;
function isInNeighborCollapsedFile(file: ContentFile, importPath: string, byFolder: KeyGroup<string, ContentFile>): boolean {
  if (importPath === "." || importPath === "./" || importPath === "./index") { return true; }
  const match = importPath.match(neighborImportRegex);
  if (match == null) { return false; }
  const files = byFolder.get(file.folder);
  return files?.some(f => f.name === match[1]) || false;
}

function cutLastSegment(importPath: string): string {
  const split = importPath.split("/");
  split.pop();
  return split.join("/");
}
