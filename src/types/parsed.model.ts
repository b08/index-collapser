import { TypesModel } from "@b08/type-parser";
import { ContentFile } from "./contentFile.type";

export type ParsedModel<T extends ContentFile = ContentFile> = TypesModel<T>;
