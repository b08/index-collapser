export type Quotes = "'" | "\"";
export type LineFeed = "\n" | "\r\n" | "\r";

// this type is combined options needed for both parsing with @b08/type-parser (aliasMap)
// and generating with @b08/razor output (linefeed and quotes)
export interface GeneratorOptions {
  linefeed?: LineFeed;
  quotes?: Quotes;
}
