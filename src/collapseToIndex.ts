
import { groupBy } from "@b08/flat-key";
import { collapseGroup } from "./collapseGroup";
import { GeneratorOptions, ContentFile } from "./types";

const defaultOptions: GeneratorOptions = {
  quotes: "\"",
  linefeed: "\n"
};

export function collapseToIndex<T extends ContentFile>(files: T[], options: GeneratorOptions = {}): T[] {
  options = { ...defaultOptions, ...options };

  const byFolder = groupBy(files, f => f.folder);
  const indexes = byFolder.values().map(grp => collapseGroup(grp, byFolder, options));
  return indexes as T[];
}
