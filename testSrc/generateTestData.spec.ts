import { MultiFileConverter, transformRange } from "@b08/gulp-transform";
import fse from "fs-extra";
import { dest, src } from "gulp";
import { collapseToIndex } from "../src";

generateTestData(files => collapseToIndex(files))
  .catch(err => {
    console.error(err);
    process.exitCode = -1;
  });

async function generateTestData(generator: MultiFileConverter): Promise<void> {
  await fse.remove("./testData");
  const folders = await fse.readdir("./testDataSrc"); // ["multiLevelReferencingSameType"];
  await Promise.all(folders.map(folder => generateInFolder(folder, generator)));
}

function generateInFolder(folder: string, generator: MultiFileConverter): Promise<void> {
  const stream = src(`./testDataSrc/${folder}/**/*.ts`)
    .pipe(transformRange(files => generator(files)))
    .pipe(dest(`./testData/${folder}`));

  return waitForStream(stream);
}

export function waitForStream(stream: NodeJS.ReadWriteStream): Promise<void> {
  return new Promise((resolve, reject) => {
    stream.on("error", reject);
    stream.on("end", resolve);
  });
}
