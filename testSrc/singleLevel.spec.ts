import { test } from "@b08/test-runner";

test("single level", async expect => {
  // arrange
  const srcFile = "../testData/singleLevel";
  const target = await import(srcFile);

  // act
  const result = target.func1();

  // assert
  expect.equal(result, 5);
});

test("single level referencing index", async expect => {
  // arrange
  const srcFile = "../testData/singleLevelReferencingIndex";
  const target = await import(srcFile);

  // act
  const result = target.func1();

  // assert
  expect.equal(result, 6);
});
