import { test } from "@b08/test-runner";

test("multi level", async expect => {
  // arrange
  const srcFile = "../testData/multiLevel";
  const target = await import(srcFile);

  // act
  const result = target.func1();

  // assert
  expect.equal(result, 14);
});


test("multi level referencing index", async expect => {
  // arrange
  const srcFile = "../testData/multiLevelReferencingIndex";
  const target = await import(srcFile);

  // act
  const result = target.func1();

  // assert
  expect.equal(result, 14);
});


test("multi level with neighbor folder reference", async expect => {
  // arrange
  const srcFile = "../testData/multiFolder";
  const target = await import(srcFile);

  // act
  const result = target.func1();

  // assert
  expect.equal(result, 27);
});


test("multi level with type referenced from 2 files in same folder", async expect => {
  // arrange
  const srcFile = "../testData/multiLevelReferencingSameType";
  const target = await import(srcFile);

  // act
  const result = target.func1();

  // assert
  expect.equal(result, 9);
});
