import { func2 } from "./func2";
import { func3 } from "./level2/func3";

export function func1(): number {
  return 1 + func2() + func3();
}
