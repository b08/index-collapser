export * from "./func2";
import { func1 } from "./func1";

export function func5(): number {
  return func1() + 1;
}
