import { func6 } from "../neighbor";
import { func5 } from "../neighbor/func5";

export function func4(): number {
  return 5 + func5() + func6();
}
