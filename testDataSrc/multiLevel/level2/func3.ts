import { func4 } from "./func4";

export function func3(): number {
  return 4 + func4();
}
