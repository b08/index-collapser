import { func2 } from "./func2";

export function func1(): number {
  return 2 + func2();
}
