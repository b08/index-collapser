import { func3 } from "./level2/func3";

export function func2(): number {
  return 3 + func3();
}
