import { func2 } from ".";

export function func1(): number {
  return 2 + func2();
}
